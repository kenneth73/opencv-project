from facenet_pytorch import MTCNN
import cv2 as cv
import torch
from coordenada import Coordenada
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

mtcnn = MTCNN(keep_all=True, device=device)

capture = cv.VideoCapture(0)
# capture.set(3, 1280)
# capture.set(4, 720)




while True:
    isTrue, frame = capture.read()
    # gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    boxes, probs, points = mtcnn.detect(frame,landmarks=True)

    if boxes is not None:
        for (box, point) in zip(boxes, points) or []:
            (x, y, x2, y2) = box.astype('int')
            p1 = Coordenada(x, y)
            p2 = Coordenada(x2, y2)
            # p2 = (x2, y2)

            frame = cv.rectangle(frame, p1.tuple(), p2.tuple(), (0, 255, 0), thickness=2)

            for p in point:
                p = tuple(p.astype('int'))
                frame = cv.circle(frame, p, 5, (0, 0, 255), thickness=2)

    cv.imshow("Video", frame)


    if cv.waitKey(20) & 0xFF==ord('d'):
        break

capture.release()
cv.destroyAllWindows()

def calcula_distancia(coord_1, coord_2):
    mov_in_x = coord_1.x - coord_2.y
    mov_in_y = coord_1.y - coord_2.y
    return f'moviment: ({mov_in_x}{mov_in_y})'

